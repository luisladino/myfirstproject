package main

import "fmt"

type Employe struct {
	id   int
	name string
	job  string
}

func (e *Employe) SetId(id int, name string, job string) {
	e.id = id
	e.name = name
	e.job = job
}

func main() {
	e := Employe{}
	fmt.Printf("%v", e)
	e.id = 1
	e.name = "Ladino"
	fmt.Printf("%v", e)
	e.SetId(5, "Laura", "developer")
	fmt.Printf("%v", e)

}
